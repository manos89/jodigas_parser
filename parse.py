import requests
import os
import json
from zipfile import ZipFile




def check_create_directory():
    if not os.path.isdir('./files'):
        os.mkdir("files")

def download_file(url):
    check_create_directory()
    local_filename = url.split('/')[-1]
    r = requests.get(url)
    with open(os.path.join("./files", local_filename), 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return local_filename


def unzip_file(filename):
    with ZipFile(os.path.join("./files", filename), 'r') as zipObj:
        zipObj.extractall()

def read_csv():
    pass

def csv_to_json():
    pass


if __name__ == "__main__":
    input_url = "https://www.jodidata.org/_resources/files/downloads/gas-data/jodi_gas_csv_beta.zip"
    filename = download_file(input_url)
    unzip_file(filename)